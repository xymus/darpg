# Devil's Avocados, a Recipe Preparation Game

## Install on GNU/Linux

Download [darpg_linux.tar.gz](https://xymus.net/pub/darpg/darpg_linux.tar.gz),
decompress and launch with `bin/darpg`.

## Install on Windows

Download [darpg_win64.zip](https://xymus.net/pub/darpg/darpg_win64.zip),
unzip and launch with `launch.bat`.

## Install on Android

Download [darpg.apk](https://xymus.net/pub/darpg/darpg.apk),
install with `adb install -rd darpg.apk`.

## Compile for GNU/Linux

* Install Nit and required system packages by following the [Nit README](https://github.com/nitlang/nit/blob/master/README.md).
* Install additional system packages for *gamnit* by following the [*gamnit* README](https://github.com/nitlang/nit/blob/master/lib/gamnit/README.md).
* Clone this repository.
* Compile with `make`.
* Launch with `bin/darpg`.

## How to play

* Move with WASD.
* Reorder avocados with Q & E.
* Tap spacebar to drop avocados.
* Hold spacebar to throw avocados.
* Collect the lime and the cilantro to open the door, enter the door to win!

## Cheats

* Select the level by an argument at launch, ex:

  ~~~raw
  bin/darpg 0 # for the tutorial
  bin/darpg 3 # for the last level
  ~~~

## Artwork

* earth.jpeg created by veeterzy, published under CC0
* heaven.jpeg created by Unsplash, published under CC0
* hell.jpg created by ZERIG, Jeroným Pelikovský, published under CC0
* button textures are created by Kenney.nl, published under CC0
* all other artwork created by Alexis Laferrière and Marie-Pier Lessard
