INKSCAPE_DIR ?= $(shell nitls -pP inkscape_tools)
GAMNIT_DIR ?= $(shell nitls -pP gamnit)

all: bin/darpg

bin/darpg: $(shell nitls -M src/darpg.nit -m linux) pre-build assets/splash.png
	nitc src/darpg.nit -m linux -o $@

bin/darpg_win64.exe: $(shell nitls -M src/darpg.nit -m linux) pre-build assets/splash.png
	LDLIBS="-lm -lSDL2_image" CC="ccache /usr/bin/x86_64-w64-mingw32-gcc-posix" NO_STACKTRACE=True \
		nitc src/darpg.nit -m linux -o $@

bin/darpg.apk: $(shell nitls -M src/darpg.nit.nit -m android -m src/ui/touch.nit) pre-build android/res/
	nitc src/darpg.nit -m android -m src/ui/touch.nit -o $@

android-release: $(shell nitls -M src/darpg.nit.nit -m android -m src/ui/touch.nit) pre-build android/res/
	nitc src/darpg.nit -m android -m src/ui/touch.nit -o $@ --release

gen/drawing.nit: art/drawing.svg
	make -C ${INKSCAPE_DIR}
	${INKSCAPE_DIR}/bin/svg_to_png_and_nit -g --src gen/ --scale 2.0 art/drawing.svg

assets/splash.png: art/splash_en.svg
	inkscape -z -f art/splash_en.svg -e $@ -C -y 0.0

gen/controls.nit: art/controls.xml
	nit ${GAMNIT_DIR}/texture_atlas_parser.nit art/controls.xml --dir gen/ -n controls

android/res/: art/icon.svg
	make -C ${INKSCAPE_DIR}
	${INKSCAPE_DIR}/bin/svg_to_icons --out android/res --android art/icon.svg

pre-build: gen/drawing.nit gen/controls.nit

check:
