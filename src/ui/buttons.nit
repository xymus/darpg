import gamnit::bmfont
intrude import gamnit::font

import controls # Generated from controls.xml

redef class App
	var assets_controls = new Controls
	var buttons = new Array[Button]

	var font = new BMFontAsset("font/font.fnt")
end

class Button
	var center: Point3d[Float]

	# FIXME update `text_sprites`
	var text: Text

	private var texture: Texture = app.assets_controls.grey_button01
	private var texture_over: Texture = app.assets_controls.grey_button04

	private var back = new Sprite(texture, center) is lazy

	private var text_sprites = new TextSprites(app.font, center.offset(0.0, 0.0, 0.1), text) is lazy

	private var left: Float = center.x - texture.width*0.5 is lazy
	private var right: Float = center.x + texture.width*0.5 is lazy
	private var top: Float = center.y + texture.height*0.5 is lazy
	private var bottom: Float = center.y - texture.height*0.5 is lazy

	private var over = false

	init
	do
		app.buttons.add self
		back.draw_order = -1
		app.ui_sprites.add back

		#text_sprites.scale = 0.75
		text_sprites.align = 0.5
		text_sprites.valign = 0.5
		text_sprites.force_redraw
	end

	fun accept_event(e: InputEvent): Bool
	do
		# TODO deal with multiple pointers
		if e isa PointerEvent then
			var new_over = false
			if e.x >= left and e.x <= right and -e.y >= bottom and -e.y <= top then
				new_over = true
				if e.depressed and not e.is_move then hit
			end

			if new_over != over then
				var dy
				if new_over then
					back.texture = texture_over
					dy = -2.0
				else
					dy = 2.0
					back.texture = texture
				end
				back.center.y += dy
				text_sprites.cached_text = null
				text_sprites.text = text
				over = new_over
			end
			return new_over
		end
		return false
	end

	# Action
	fun hit do end

	fun destroy
	do
		app.buttons.remove self
		app.ui_sprites.remove back
		for s in text_sprites.sprites do app.ui_sprites.remove s
	end
end
