import gamnit::virtual_gamepad

import darpg

redef class App
	redef fun on_create
	do
		super

		var gamepad = new VirtualGamepad
		gamepad.add_dpad
		gamepad.add_button("q", assets.turn_left)
		gamepad.add_button("e", assets.turn_right)
		gamepad.add_button("space", assets.throw)
		self.gamepad = gamepad
	end

	redef fun next_level
	do
		super
		var gamepad = self.gamepad
		if gamepad != null then gamepad.visible = true
	end

	redef fun reset_level
	do
		super
		var gamepad = self.gamepad
		if gamepad != null then gamepad.visible = true
	end
end
