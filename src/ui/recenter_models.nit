import gamnit::depth

redef class Model
	var recentered = false

	fun recenter
	do
		if recentered then return
		recentered = true

		var center = center
		var min = min
		for leaf in leaves do
			var mesh = leaf.mesh

			var i = 0
			var m = 1.0
			var vertices = mesh.vertices
			while i < vertices.length do
				vertices[i  ] -= center.x * m
				vertices[i+1] -= center.y * m
				vertices[i+2] -= center.z * m
				i += 3
			end
		end
	end

	fun offset(dz: nullable Float)
	do
		if dz == null then dz = 0.0
		for leaf in leaves do
			var mesh = leaf.mesh

			var i = 0
			var vertices = mesh.vertices
			while i < vertices.length do
				#vertices[i  ] -= center.x * m
				#vertices[i+1] -= center.y * m
				vertices[i+2] += dz
				i += 3
			end
		end
	end
end
