import app

redef class App
	private var story_asset = new TextAsset("story_text.txt") is lazy

	# Load the level to play on it
	var story_text: Array[String] is lazy do
		var data = story_asset.load
		var error = story_asset.error
		if error != null then
			print_error "Error loading asset at '{story_asset.path}': {error}"
			return new Array[String]
		end

		var arr = new Array[String]
		var buf = ""
		for line in data.split("\n") do
			if line.is_empty then
				if not buf.is_empty then arr.add buf
				buf = ""
			else
				if not buf.is_empty then buf += "\n"
				buf += line
			end
		end
		if not buf.is_empty then arr.add buf
		return arr
	end
end
