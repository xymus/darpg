import geometry

import app
import more_collections

class Campaign
	var levels: Array[LevelInfo] = [
		new LevelInfo("tutorial"),
		new LevelInfo("earth"),
		new LevelInfo("heaven"),
		new LevelInfo("hell")]

	fun next_level_after(level: Level): LevelInfo
	do
		var i = levels.index_of(level.info)
		assert i != -1
		return levels.modulo(i+1)
	end
end

# Metadata on a (not yet loaded) level
class LevelInfo
	# Short name, corresponds to the file name in `assets/levels/name.txt`
	var name: String

	# Readable asset
	private var asset = new TextAsset("levels" / name + ".txt") is lazy

	# Order and type of avocados, set by `load`
	#
	# Example: "sshslo"
	var avocados_order = ""

	# Leader starting position, set by `load`
	var start_position = new Point[Int](0, 0)

	# Y coordinate of the lowest tile, set by `load`
	private var lowest_y = 0.0

	# Limit to kill the leader when falling
	fun deadly_y: Float do return lowest_y - 16.0

	# Load the level to play on it
	fun load: Level
	do
		var lvl = new Level(self)

		var data = asset.load
		var error = asset.error
		if error != null then
			print_error "Error loading asset at '{asset.path}': {error}"
			return lvl
		end

		# Detect X coordinate to begin the level so it is centered
		var ox = 0
		var lines = data.split('\n')
		for line in lines do
			if line.has_prefix("%") then continue
			ox = ox.min(-line.length/2)
		end

		# Parse level
		var y = 0
		for line in data.split('\n') do
			if line.has_prefix("%") then
				# Avocados ordering line
				avocados_order = line.substring_from(1)
				continue
			end

			var x = ox
			for c in line.chars do
				if c == '#' then
					# Hard tile
					lvl.grid[x, y] = new GroundTile(lvl, new Point3d[Float](x.to_f, y.to_f, 0.1))
				else if c == 'G' then
					# Growable soil / tile
					lvl.grid[x, y] = new GrowableTile(lvl, new Point3d[Float](x.to_f, y.to_f, 0.1))
				else if c == 'S' then
					# Starting position
					start_position.x = x
					start_position.y = y
				else if c == 'E' then
					# Exit/ending position
					lvl.exit.center.x = x.to_f
					lvl.exit.center.y = y.to_f
				else if c == 'c' then
					# Ingredient cilantro
					lvl.ingredients.add new Cilantro(lvl, new Point3d[Float](x.to_f, y.to_f, -0.1))
				else if c == 'l' then
					# Ingredient lime
					lvl.ingredients.add new Lime(lvl, new Point3d[Float](x.to_f, y.to_f, -0.1))
				else if c == 'N' then
					# Enemy nacho
					lvl.ennemies.add new Nacho(lvl, new Point3d[Float](x.to_f, y.to_f, -0.1))
				else if c == 'B' then
					# Enemy bean
					lvl.ennemies.add new Bean(lvl, new Point3d[Float](x.to_f, y.to_f, -0.1))
				else if c == 'X' then
					# Boss
					lvl.ennemies.add new Boss(lvl, new Point3d[Float](x.to_f, y.to_f, -0.1))
				end
				x += 1
			end
			y -= 1
		end
		lowest_y = y.to_f-1.0

		return lvl
	end
end

# Loaded level for play
class Level
	# Metadata on this level (for resets mainly)
	var info: LevelInfo

	# Grid of tiles
	var grid = new Grid

	# All entities bound to this level
	var entities = new Array[Entity]

	# Ingredients still on the map
	var ingredients = new Array[Ingredient]
	var ennemies = new Array[Ennemy]

	# Exit "door", closed until `ingredients.is_empty`
	var exit = new Door(self, new Point3d[Float](0.0, 0.0, 0.0))
end

class Grid
	super HashMap2[Int, Int, Tile]

	redef fun []=(x, y, v)
	do
		var t = self[x,y]
		if t != null and t != v then t.destroy
		super
	end
end

abstract class Tile
	super Entity

	var splashed = false

	fun splash do splashed = true
end

class GroundTile
	super Tile
end

class GrowableTile
	super Tile
end

class TreeTile
	super Tile
	var angle: Float
end

# Something in the world
class Entity
	var level: Level

	init do level.entities.add self

	var center: Point3d[Float]

	# Remove from the world
	fun destroy do level.entities.remove self
end

# Something in the world that may move or act at each game logic turn
class ActiveEntity
	super Entity

	# Update game logic after a time lapse of `dt` seconds
	fun update(dt: Float) do end
end

abstract class Ingredient
	super Entity
end

class Lime
	super Ingredient
end

class Cilantro
	super Ingredient
end
# Level Ennemies
class Ennemy
	super ActiveEntity
	var life = 5
	var move_speed = 1.0
	var current = 0
	var distance : Int = 2 * 55
	var direction = 1.0
	var stuck = false

	fun guard(dt, dx: Float)
	do
		if stuck then return
		var current_tile = level.grid[center.x.to_i, center.y.to_i - 1]
		if current_tile != null and current_tile.splashed then
			stuck = true
			return
		end

		dx = dt*dx*move_speed

		if current != distance then
			# continue to go further
			center.x += dx * direction
			current += 1
		else
			# change direction
			direction = direction * -1.0
			center.x += dx * direction
			current = 0
		end
	end

	fun hit(damage: Int) do
		self.life -= damage
		if self.life <= 0 then destroy
	end

	redef fun destroy do
		super
		level.ennemies.remove self
	end
end

class Nacho
	super Ennemy
end

class Bean
	super Ennemy

	redef var move_speed = 2.0
	redef var life = 10
end

class Boss
	super Nacho

	redef var life = 30
end

# Level exit
class Door
	super Entity

	var open = false is writable
end
