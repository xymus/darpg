import levels
intrude import gamnit::cameras

redef class ActiveEntity

	var move_speed = 4.0
	private var falling_speed = 0.0
	private var falling_accel: Float = -18.0

	private var falling_speed_x = 0.0

	# Orientation of the last move: left -1.0, right 1.0
	var heading = 1.0

	fun move(dt, dx, dy: Float)
	do
		if dx != 0.0 then heading = dx/dx.abs

		dx = dt*dx*move_speed
		dy = dt*dy*move_speed

		# If falling, move slower on X
		var down_tile = level.grid[center.x.round.to_i, (center.y-0.5).round.to_i]
		if down_tile == null or (self isa ThrowableEntity and down_tile isa TreeTile) then
			dx *= 0.5
			dx += falling_speed_x*dt
		end

		var in_tile = level.grid[(center.x+dx).round.to_i, (center.y+dy-0.4).round.to_i]
		if in_tile isa TreeTile and not self isa ThrowableEntity then
			# Leader climb tree
			center.x += dx
			center.y += dy
			dy = 0.0
		else if in_tile == null or (self isa ThrowableEntity and in_tile isa TreeTile) then
			# Move OK
			center.x += dx

			down_tile = level.grid[center.x.round.to_i, (center.y-1.0).round.to_i]
			if down_tile isa TreeTile then
				center.y += dy
			end
			dy = 0.0
		else
			# Step up on tile?
			dy += 1.0
			var up_tile = level.grid[(center.x+dx).round.to_i, (center.y+dy+0.4).round.to_i]
			if up_tile == null then
				# Go ahead
				center.x += dx
				center.y = in_tile.center.y + 1.0
			end
			dx = 0.0
		end

		down_tile = level.grid[center.x.round.to_i, (center.y-0.5).round.to_i]
		if down_tile == null  or (self isa ThrowableEntity and down_tile isa TreeTile) then
			# Fall
			falling_speed += dt*falling_accel
			dy = dt*falling_speed
			center.y += dy
		else if falling_speed != 0.0 then
			# Land
			falling_speed = 0.0
			falling_speed_x = 0.0
			center.y = down_tile.center.y + 1.0
			land down_tile
		end
	end

	# Juste landed on `tile`
	fun land(tile: Tile) do end
end

class ThrowableEntity
	super ActiveEntity

	var dx = 0.0
	var dy = 0.0

	fun throw(force, angle: Float, from: Leader)
	do
		force *= 0.2

		var max_force = 1.1
		force = force.clamp(-max_force, max_force)

		# The values are used to center the throw with the top of the fork
		center.x = from.center.x - 0.6
		center.y = from.center.y + 2.6

		var fx = -angle.cos * force
		var fy = -angle.sin * force
		#print "throw {fx} {fy}"
		falling_speed = 20.0*fy - 4.0
		falling_speed_x = 14.0*fx
	end
end

class Leader
	super ActiveEntity

	# Ingredients found and removed from the map
	var collected_ingredients = new Array[Ingredient]
	var avocados = new Array[Avocado]
	var current_avocado: nullable Avocado is writable
	var next_avocado: nullable Avocado is writable
	var last_positions = new TimedArray[TimedPosition](5.0)
	var dead = false is writable
	var parts = new Array[LeaderPart]
	redef fun move(dt, dx, dy)
	do
		if dead then return

		var lpos = new Point3d[Float](center.x, center.y, center.z)

		super

		if lpos != center or last_positions.is_empty then
			lpos = new Point3d[Float](center.x, center.y, center.z)
			last_positions.add(new TimedPosition(lpos, dt, falling_speed != 0.0))
		else
			# Let the avocados fall
			var prev = last_positions.last
			for pos in last_positions.reverse_iterator, i in [0..last_positions.length[ do
				if pos.in_air then
					prev.dt += pos.dt
					break
				end
				prev = pos
			end
		end

		for a in avocados do a.follow_leader self

		# Collect ingredients?
		for ingredient in level.ingredients.reverse_iterator do
			if center.dist2(ingredient.center) <= 1.0 then
				collect ingredient
			end
		end

		for ennemy in level.ennemies.reverse_iterator do if center.dist2(ennemy.center) <= 0.4 then dead = true

		if center.y < level.info.deadly_y then dead = true

		# Win?
		if level.exit.open and center.dist2(level.exit.center) <= 0.2 then
			level.won = true
		end
	end

	# When in switching animation, we still need to update some things ...
	fun animation_update(dt: Float) do
		for a in avocados do a.follow_leader(self)
		for ennemy in level.ennemies.reverse_iterator do if center.dist2(ennemy.center) <= 0.4 then dead = true
		for a in avocados do a.update(dt)
	end

	fun collect(ingredient: Ingredient)
	do
		collected_ingredients.add ingredient
		level.ingredients.remove ingredient
	end

	fun throw_next_avocado(force, angle: Float)
	do
		var a = current_avocado
		if a != null then
			level.avocados_in_freefall.add(a)
			a.throw(force, angle, self)
			if not avocados.is_empty then current_avocado = avocados.shift else current_avocado = null
		end
	end

	fun rotate(wise: Bool) do
		if avocados.length <= 0 then return
		var a = current_avocado
		if a == null then return
		var r = a.rotate_order

		for i in [1..a.total_avocado_type - 1] do
			var next_rotate = 0
			if wise then next_rotate = (r + i) % a.total_avocado_type
			if not wise then
				next_rotate = r - i
				if next_rotate < 0 then next_rotate += a.total_avocado_type
			end
			for na in avocados do
				if next_rotate == na.rotate_order then
					self.next_avocado = na
					return
				end
			end
		end
	end
end

class LeaderPart
	super Entity

	fun update(dt, dx, dy: Float, leader: Leader) do end
end

class DevilHead
	super LeaderPart
end

class DevilFork
	super LeaderPart
end

abstract class Avocado
	super ThrowableEntity

	var leader: nullable Leader is noinit
	var damage = 1
	var landed = false

	fun follow_leader(leader: Leader) do
		var pos = leader.avocados.index_of(self) + 1
		var delay = pos.to_f * 0.2
		var new_pos = leader.last_positions.get_since_dt(delay)
		self.center.x = new_pos.x
		self.center.y = new_pos.y
		#self.center.z = new_pos.z # Keep depth
	end

	redef fun move(dt, dx, dy) do
		super
		if landed then return
		for e in level.ennemies.reverse_iterator do
			if center.dist2(e.center) <= 0.8 then
				e.hit(damage)
				destroy
			end
		end
	end

	redef fun land(tile) do
		if not tile isa TreeTile then
			landed = true
			destroy
		end
	end

	redef fun destroy do
		super
		level.avocados_in_freefall.remove self
	end

	fun rotate_order: Int do return 0
	fun total_avocado_type: Int do return 4
end

class HardAvocado
	super Avocado
	redef var damage = 5

	redef fun rotate_order do return 1
end

class SoftAvocado
	super Avocado

	redef fun land(tile) do
		super
		if tile isa GrowableTile then
			level.entities.add(new Tree(level, tile.center))
		else if not tile isa TreeTile then
			tile.splash
		end
	end

	redef fun rotate_order do return 0
end

class OldAvocado
	super Avocado

	redef fun rotate_order do return 3

	redef fun land(tile) do
		super
		level.grid.remove_at(tile.center.x.round.to_i, tile.center.y.round.to_i)
		tile.destroy
	end
end

class LawyerAvocado
	super Avocado
	# Because lawyers avocados are badass
	redef var damage = 10

	redef fun rotate_order do return 2
end

class Tree
	super Entity

	var height = 8

	redef init do
		var direction = 0.0
		var x = center.x.round.to_i
		var y = center.y.round.to_i
		var left = level.grid[x - 1, y]
		var right = level.grid[x + 1, y]
		if left == null then direction = -1.0
		if right == null then direction = 1.0

		var angle = (-pi / 2.0) * direction

		for i in [1..height] do
			var dx = i.to_f * direction
			var dy = 0.0
			if direction == 0.0 then dy = 1.0 * i.to_f
			x = (center.x + dx).round.to_i
			y = (center.y + dy).round.to_i
			var tile = level.grid[x, y]
			if tile == null then
				var tree_tile = new TreeTile(level, new Point3d[Float](x.to_f, y.to_f, 0.1), angle)
				level.grid[x, y] = tree_tile
			else
				height = i - 1
				break
			end
		end
	end
end

class TimedPosition
	var p: Point3d[Float]
	var dt: Float
	var in_air: Bool
end

class TimedArray[E: TimedPosition]
	super List[E]

	var max_time : Float
	var total_time: Float = 0.0

	redef fun add(item) do
		super
		total_time += item.dt
		var e: E
		if total_time > max_time then
			e = self.shift
			total_time -= e.dt
		end
	end

	fun get_since_dt(dt: Float): Point3d[Float] do
		var time = 0.0
		var pe = last
		for e in self.reverse_iterator do
			time += e.dt
			if time >= dt then return pe.p
			pe = e
		end
		return self.first.p
	end
end

# ---
# Redef levels

redef class Level
	var leader: Leader is noinit
	var avocados_in_freefall = new Array[Avocado]

	var won = false is writable

	fun update(dt: Float)
	do
		if ingredients.is_empty then
			# Open door
			exit.open = true
		end

		for a in avocados_in_freefall do a.move(dt, 0.0, 0.0)
	end
end

redef class LevelInfo
	redef fun load
	do
		var lvl = super

		var sx = start_position.x.to_f
		var sy = start_position.y.to_f

		var leader_depth = 2
		var leader = new Leader(lvl, new Point3d[Float](sx, sy, leader_depth.to_f*0.1))
		lvl.leader = leader

		# Parse avocados order from line
		for c in avocados_order do
			var depth = 10.rand - 5
			if depth == leader_depth then depth = -5

			var a: nullable Avocado = null
			if c == 's' then
				a = new SoftAvocado(lvl, new Point3d[Float](sx, sy, depth.to_f*0.1))
			else if c == 'h' then
				a = new HardAvocado(lvl, new Point3d[Float](sx, sy, depth.to_f*0.1))
			else if c == 'o' then
				a = new OldAvocado(lvl, new Point3d[Float](sx, sy, depth.to_f*0.1))
			else if c == 'l' then
				a = new LawyerAvocado(lvl, new Point3d[Float](sx, sy, depth.to_f*0.1))
			end

			if a != null then
				leader.avocados.add(a)
				a.leader = leader
			end
		end
		leader.current_avocado = leader.avocados.shift
		return lvl
	end
end
