# Devil's avocados - Recipe Preparation Game
module darpg is
	app_name "Devil\\'s avocados"
	app_namespace "net.xymus.darpg"
	app_version(0, 1, git_revision)

	android_manifest_activity """android:screenOrientation="sensorLandscape""""
end

import gamnit::depth
import gamnit::keys
import app::audio
import performance_analysis

intrude import gamnit::cameras

import game

import buttons
import story_parser
import recenter_models

import drawing # Generated from drawing.svg

redef class Model
	fun search(prefix: String): Model
	do
		var map = named_parts
		for k, v in map do
			if k.has(prefix) then return v
		end

		print_error "Prefix 'prefix' not in this model: {map.keys}"
		abort
	end
end

redef class SmoothMaterial
	redef fun ambient_color do return diffuse_color
end

redef class App

	# ---
	# Assets

	var assets = new DrawingImages

	var models = new Model("models.obj")

	var model_growable = new LeafModel(new UVSphere(0.66, 12, 6),
		new SmoothMaterial([0.0]*4, [0.2, 0.1, 0.05, 1.0], [0.0]*4))

	var model_splash = new LeafModel(new UVSphere(0.6, 12, 6),
		new SmoothMaterial([0.0]*4, [0.0, 0.6, 0.0, 1.0], [0.0]*4))

	var model_plant: Model = models.search("plant") is lazy

	var model_backgrounds: Array[nullable Model] =
		[models.search("hell_back"), models.search("grass_back"), models.search("cloud_back")] is lazy

	var model_tile: Array[Model] = [
		models.search("hell_1x1"),
		models.search("grass_1x1"),
		models.search("cloud_1x1")] is lazy

	# Index of assets to use (0 for tutorial, 1 for earth, etc.)
	var assets_level_index = 0

	# Sound effects
	#private var fx = new Sound("sounds/fx.mp3")

	private var background_texture : Array[Texture] = [new Texture("hell.jpg"), new Texture("earth.jpeg"), new Texture("heaven.jpeg"), new Texture("hell.jpg")]

	private var background = new Sprite(background_texture[0], new Point3d[Float](0.0, 0.0, -1200.0)) is lazy

	private var splash_texture = new Texture("splash.png")
	private var splash_sprite: nullable Sprite = new Sprite(splash_texture,
		new Point3d[Float](ui_camera.width*0.5,ui_camera.height*-0.5,0.0)) is lazy

	# ---
	# Play
	var campaign = new Campaign is lazy
	var level: nullable Level = null

	var switching: nullable SwitchGesture is noautoinit

	redef fun create_gamnit
	do
		super
		glClearColor(0x7d.to_f/255.0, 0xbd.to_f/255.0, 0xf9.to_f/255.0, 1.0)
		glCullFace gl_BACK
	end

	redef fun create_scene
	do
		super

		# Show splash right away
		ui_sprites.add splash_sprite.as(not null)

		# Config camera
		world_camera.near = 1.0
		world_camera.reset_height 15.0

		background.draw_order = -100
		sprites.add background

		# Accept a level index on the command line
		var index = 0
		if args.not_empty and args.first.is_int then index = args.first.to_i

		# Show intro text
		show_story index

		# Prepare tutorial
		for s in tutorial_text_sprites.sprites do ui_sprites.remove s

		# Load an prepare all models
		models.load
		assert models.errors.is_empty

		for m in models.named_parts.values do m.recenter
		model_plant.offset(dz=-0.02)
		print "# Models"
		print models.named_parts.keys.join("\n")

		# Light
		light.position.y = 32.0
		light.position.z = 8.0
	end

	redef fun update(dt)
	do
		var level = level
		if level != null then
			if not level.won then
				var dx = 0.0
				var dy = 0.0

				for k in pressed_keys do
					if k == "left" or k == "a" then
						dx -= 1.0
					else if k == "right" or k == "d" then
						dx += 1.0
					else if k == "up" or k == "w" then
						dy += 1.0
					else if k == "down" or k == "s" then
						dy -= 1.0
					end
				end

				var s = switching
				if s != null then
					s.update(dt)
					level.leader.animation_update(dt)
					for e in level.ennemies do e.guard(dt, 1.0)
					return
				end

				# Player movement and logic
				level.leader.move(dt, dx, dy)
				for p in level.leader.parts do p.sprite.needs_update
				level.leader.update_current_avocado

				for a in level.leader.avocados do a.update(dt)

				# ennemies movement
				for e in level.ennemies do e.guard(dt, 1.0)

				# Update camera position
				var camera_margin = 4.0
				var camera_margin_y = 2.0
				var cx = world_camera.position.x - level.leader.center.x
				var cy = world_camera.position.y - level.leader.center.y
				if cx.abs > camera_margin then world_camera.position.x -= cx - (cx/cx.abs)*camera_margin
				if cy.abs > camera_margin_y then world_camera.position.y -= cy - (cy/cy.abs)*camera_margin_y
				world_camera.position.y = level.leader.center.y + 1.0
			end

			# Core game logic
			level.update dt

			if level.won and but_next_level == null then
				# Show the story text
				show_story next_level_index + 1

			end

			# Tutorial
			var leader = level.leader
			#print leader.center.x
			if assets_level_index == 0 then
				# Tuto
				if leader.center.x < -26.0 then
					tutorial_text_sprites.text = """
Use WASD to move around."""
				else if leader.center.x < -20.0 then
					tutorial_text_sprites.text = """
You must collect both the lime and the cilantro in each level."""
				else if leader.center.x < -16.2 then
					tutorial_text_sprites.text = """
Ripe avocados can be planted as climbable trees.
Tap space to drop the first avocado in line."""
				else if leader.center.x < -14.5 then
					tutorial_text_sprites.text = """
The trees grow sideways when on a cliff."""
				else if leader.center.x < -7.0 then
					tutorial_text_sprites.text = """
Hold space to throw and plant an avocado further away."""
				else if leader.center.x < -3.0 then
					tutorial_text_sprites.text = """
You also have young avocados, to move them first
tap Q or E to reorder them."""
				else if leader.center.x < 0.0 then
					tutorial_text_sprites.text = """
Young avocados hit nachos the hardest!
Ripe avocados also hurt nachos, but they have other uses.
"""
				else if leader.center.x < 5.0 then
					tutorial_text_sprites.text = """
Ripe avocados hitting the ground can make enemies slip and fall.
"""
				else if leader.center.x < 15.0 then
					tutorial_text_sprites.text = """
You will encounter other avocados, some are old some are tough...
"""
				else if leader.center.x < 20.0 then
					tutorial_text_sprites.text = """
Beans are tough!
"""
				else
					tutorial_text_sprites.text = """
Go through the door to finish the level.
It opens when you have collected the two ingredients.
"""
				end
			end
		end


		var throw = throw_gesture
		if throw != null then
			throw.update(dt)
		end
	end

	private var virtual_avocado: nullable VirtualAvocado = null
	private var virtual_trace = new Array[Sprite]

	# Show store before, in between and after each level
	fun show_story(level: Int)
	do
		for s in tutorial_text_sprites.sprites do ui_sprites.remove_all s

		var ending = level == campaign.levels.length

		# Story/explanation text
		var text = story_text[level]
		story_text_sprites.text = text

		# "Continue" button
		var but_text = if level == 0 then "Let's go" else if ending then "Restart" else "Continue"
		var but = new NextLevelButton(ui_camera.bottom.offset(240.0, 120.0, 0.0), but_text)
		but_next_level = but

		# Prepare loading the next level
		next_level_index = level
		if ending then next_level_index = 0
	end
	private var next_level_index = 0

	# Remove story text and "continue" button added by `show_story`
	fun clear_story
	do
		var splash_sprite = splash_sprite
		if splash_sprite != null then
			ui_sprites.remove_all splash_sprite
			self.splash_sprite = null
		end

		var but_next_level = but_next_level
		if but_next_level != null then
			but_next_level.destroy
			self.but_next_level = null
		end
		for s in story_text_sprites.sprites do app.ui_sprites.remove_all s
	end

	private var story_text_sprites = new TextSprites(font, ui_camera.bottom_left.offset(480.0, 320.0, 0.0), "") is lazy
	private var but_next_level: nullable NextLevelButton = null
	private var but_reset_level = new ResetButton(ui_camera.bottom.offset(-240.0, 120.0, 0.0), "Reset level") is lazy

	private var tutorial_text_sprites = new TextSprites(font, ui_camera.top_left.offset(320.0, -240.0, 0.0), "") is lazy

	# Restart the current level
	fun reset_level
	do
		clear_story
		var level = level
		assert level != null
		for e in level.entities.reverse_iterator do e.destroy
		app.actors.clear
		level = level.info.load
		self.level = level

		var virtual_avocado = new VirtualAvocado(level, new Point3d[Float](0.0, 0.0, 0.0))
		self.virtual_avocado = virtual_avocado

		setup_level_3d
	end

	# Load and display the next level
	fun next_level
	do
		var index = next_level_index

		var level = level
		if level != null then
			# Clean up old level
			for e in level.entities.reverse_iterator do e.destroy
		end
		app.actors.clear

		# Ready new assets
		assets_level_index = next_level_index

		var next_level_info = campaign.levels.modulo(index)
		level = next_level_info.load
		self.level = level

		but_reset_level # Lazy load "Reset" button

		var virtual_avocado = new VirtualAvocado(level, new Point3d[Float](0.0, 0.0, 0.0))
		self.virtual_avocado = virtual_avocado

		setup_level_3d
	end

	private fun setup_level_3d
	do
		var index = assets_level_index
		var background_model = model_backgrounds.modulo(index)
		if background_model != null then
			var back_actor = new Actor(background_model, new Point3d[Float](0.0, 0.0, -80.0))
			back_actor.scale = 36.0
			app.actors.add back_actor
			app.sprites.remove_all background
		else
			background.texture = background_texture[index]
			background.scale = 1.0
			if index == 2 then background.scale = 4.0
			app.sprites.add background
		end

		if index == 0 then
			var actor = new Actor(models.search("hell_s0"),
				new Point3d[Float](-23.4, -5.2, 0.0))
			actor.scale = 13.3
			app.actors.add actor
		end
	end

	redef fun accept_event(event)
	do
		if super then return true

		if switching != null then return false

		for button in buttons.reverse_iterator do if button.accept_event(event) then return true

		if event isa QuitEvent then
			print sys.perfs
			exit 0
		else if event isa KeyEvent and event.is_up then
			if event.name == "escape" then
				print sys.perfs
				exit 0
			#else if event.name == "r" then
				#app.reset_level
			else if event.name == "e" then
				var level = level
				if level != null then level.leader.rotate true
			else if event.name == "q" then
				var level = level
				if level != null then level.leader.rotate false
			end
		else if event isa KeyEvent and event.is_down then
			if event.name == "space" then
				var level = level
				if level != null and level.leader.avocados.not_empty and
				   space_pressed_clock == null and not level.leader.dead then
					space_pressed_clock = new Clock
				end
			end

		else if event isa PointerEvent then

			var level = level
			if level != null then
				var throw = throw_gesture
				if event.is_move and throw != null then
					throw.current.x = event.x
					throw.current.y = event.y
				else if event.pressed then
					if throw == null then
						throw = new ThrowGesture
						self.throw_gesture = throw
					end

					throw.start.x = event.x
					throw.start.y = event.y
				else if event.depressed and throw != null then
					self.throw_gesture = null
				end
			end
		end

		return false
	end

	private var throw_gesture: nullable ThrowGesture = null

	# Time the spacebar is pressed down to throw an avocado
	private var space_pressed_clock: nullable Clock = null
end

class ThrowGesture

	# Total time (not really used)
	var duration = 0.0

	# UI coordinates from the start point (not really used)
	var start = new Point[Float]

	# Latest UI pointer coordinates
	var current = new Point[Float]

	# Current angle, needed for the fork orientation
	var angle: Float = 0.0

	fun update(dt: Float)
	do
		duration += dt

		var avocado = app.virtual_avocado
		if avocado != null then
			# Preview throw

			var level = app.level
			if level == null then return
			if not level.leader.dead then
				var ang_str = angle_and_strength(level.leader)

				var tile = avocado.simulate_throw(ang_str.second, ang_str.first)
				var sprite = avocado.sprite
				if sprite != null and not app.sprites.has(sprite) then app.sprites.add sprite
			end
		end
	end

	# Complete throw and clean up
	fun finish
	do
		var level = app.level
		if level == null then return

		if not level.leader.dead then
			var ang_str = angle_and_strength(level.leader)
			level.leader.throw_next_avocado(ang_str.second, ang_str.first)
		end

		# Remove preview
		var avocado = app.virtual_avocado
		if avocado != null then
			app.sprites.remove_all avocado.sprite
			for s in app.virtual_trace do app.sprites.remove_all s
		end
		level.leader.fork.last_throw = 0.0
	end

	fun angle_and_strength(leader: Leader): Couple[Float, Float]
	do
		var source = leader.center
		#var source = start
		var current = app.world_camera.camera_to_world(current.x, current.y)
		var dy = current.y-source.y
		var dx = current.x-source.x
		var angle = atan2(dy, dx)
		self.angle = angle
		leader.fork.last_throw = 0.0
		var dist = dx.hypot_with(dy)
		return new Couple[Float, Float](angle, dist)
	end
end

redef class Entity

	var sprite: nullable Sprite = null

	var actors = new Array[Actor]

	init do add_to_scene

	fun add_to_scene
	do
		var model = model
		if model != null then
			var actor = new Actor(model, center)
			if app.assets_level_index != 1 then actor.roll = (4.rand).to_f * pi / 2.0
			actor.scale = 12.5
			app.actors.add actor
			self.actors.add actor
		else
			var sprite = new Sprite(texture, center)
			self.sprite = sprite
			sprite.scale = 0.02
			sprite.draw_order = draw_order
			app.sprites.add sprite
		end
	end

	# Draw order, higher values cause this sprite to be drawn latter
	fun draw_order: Int do return 0

	fun texture: Texture do return new CheckerTexture

	fun model: nullable Model do return null

	redef fun destroy
	do
		super

		var sprite = sprite
		if sprite != null then
			app.sprites.remove_all sprite
			self.sprite = null
		end

		for actor in actors do
			app.actors.remove_all actor
		end
		actors.clear
	end
end

redef class DevilHead
	redef fun texture do return app.assets.devil_head

	redef fun draw_order do return 3

	redef fun update(dt, dx, dy, leader)
	do
		var sprite = sprite
		assert sprite != null

		sprite.center.as(OffsetPoint3d).offset_x = -(leader.sprite.rotation - pi/2.0).cos * 1.2
	end
end

redef class DevilFork
	redef fun texture do return app.assets.fork

	redef fun draw_order do return 4

	# How long after a shot for the fork to get back in place
	var backing_time = 0.1

	# Getting a hang of the throwgesture to be able to finish it when the time is right
	var throw: nullable ThrowGesture is noautoinit

	# The top of the fork(used for switching avocados)
	var top: Point3d[Float] is noinit

	# Time since the last throw
	var last_throw = 0.0
	# Angle used to throw the last avocado
	var last_throw_angle = 0.0

	init do
		top = new OffsetPoint3d(center, -0.25, 1.2, 0.0)
	end

	# Convert an angle resulting from atan2 to an angle for the fork sprite
	fun atan2_to_fork(angle: Float): Float do
		if angle > pi / 2.0 and angle < pi then
			return angle - pi / 2.0
		else if angle >  - pi and angle < - pi / 2.0 then
			return  pi / 2.0 + (pi - angle.abs)
		else if angle > 0.0 and angle < pi / 2.0 then
			return - pi / 2.0 + angle
		else if angle < 0.0 and angle > - pi / 2.0 then
			return  - pi / 2.0 + angle
		end
		return 0.0
	end

	redef fun update(dt,dx, dy, leader) do
		var sprite = sprite
		assert sprite != null
		var throw = app.throw_gesture

		center.x = leader.center.x - 0.25
		center.y = leader.center.y + 1.5

		if throw == null and last_throw > backing_time then
			var leader_sprite = leader.sprite
			assert leader_sprite != null
			sprite.rotation = leader_sprite.rotation
		else if throw != null and last_throw == 0.0 then
			self.throw = throw
			sprite.rotation = atan2_to_fork(throw.angle)
			last_throw_angle = sprite.rotation
		else if throw == null and last_throw < backing_time then
			last_throw += dt
			sprite.rotation = last_throw_angle - ((last_throw / backing_time).min(1.0) * last_throw_angle)
			throw = self.throw
			if throw != null and sprite.rotation == 0.0 then
				# HACK: or else the throw animation plays two times
				# because the last_throw reset is in the `angle_and_strength` method of ThrowGesture
				var last_throw = self.last_throw
				throw.finish
				self.last_throw = last_throw
				self.throw = null
			end
		end
	end
end

# The class responsible for the switch animation
class SwitchGesture
	# Minimum animation time for switching avocado
	var min_anim_time = 0.5
	# Maximum animation time for switching avocado
	var max_anim_time = 1.5
	# Animation time for the fork before actually starting the real animation
	var pre_anim_time = 0.25

	# time since the animation started
	var time = 0.0

	# The actual animation time used
	var anim_time = 0.0

	# The time for the avocado to go from the fork to it's new position
	var avocado_anim_time = 0.0

	# The time for the fork to go pick up the new avocado
	var fork_animation_time = 0.0

	# Angle between the avocado to switch and the fork (used during animation)
	var angle:Float = 0.0

	# Keep a reference to the leader since he knows which avocados to switch
	var leader: Leader

	# We keep a reference to the current avocado of the leader before the throw
	# easier to code the functions manipulating it that way
	var avocado: Avocado is noinit

	# Position of the next avocado in  the list
	var index: Int is noinit

	# Starting point for the avocado being switched (on top of the fork)
	var start: Point3d[Float] is noinit

	# Ending point for the avocado being switched
	var ending: Point3d[Float] is noinit

	# The curve point uzed for bézier
	var curve_point: Point3d[Float] is noinit

	# Starting point for the throw of the fork
	var fork_start: Point3d[Float] is noinit

	# Ending point for the throw of the fork
	var fork_end: Point3d[Float] is noinit

	# The curve point for the fork
	var fcurve_point : Point3d[Float] is noinit

	# initialize what we need to do the animation properly
	init do
		# Initialize the starting point for the fork and the avocado
		var fc = leader.fork.center
		start = new Point3d[Float](fc.x, fc.y, fc.z)
		start.x -= 0.6
		start.y += 2.6
		fork_start = new Point3d[Float](fc.x, fc.y, fc.z)
		print "starting point for the fork = {fork_start}"

		# Initialize the ending point for the avocado
		var lna = leader.next_avocado
		assert lna != null
		avocado = leader.current_avocado.as(not null)
		ending = new Point3d[Float](lna.center.x, lna.center.y, lna.center.z)

		# Calculate the angle
		var dy = ending.y - fc.y
		var dx = ending.x - fc.x
		angle = leader.fork.atan2_to_fork(atan2(dy, dx))
		var a = atan2(dy, dx)
		print "angle = {angle}"

		# Initialize the ending point for the fork
		var dist = fc.dist2(leader.fork.top)
		print "distance between center fork and top fork = {dist}"
		var x = (dist * a.cos).abs + ending.x
		var y = (dist * a.sin).abs + ending.y
		fork_end = new Point3d[Float](x, y, fc.z)
		print "fork_end = {fork_end} and end = {ending}"

		# Calculate the curve point for the avocado
		dy = start.y - ending.y
		dx = start.x - ending.x
		x = start.x - (dx / 2.0)
		y = start.y + (dy / 2.0)
		curve_point = new Point3d[Float](x, y, start.z)

		# Calculate the curve point for the fork
		dy = fork_start.y - fork_end.y
		dx = fork_start.x - fork_end.x
		x = fork_start.x - (dx / 2.0)
		y = fork_start.y + (dy / 2.0)
		fcurve_point = new Point3d[Float](x, y, fc.z)

		# Determinate the animation time based on the distance to the leader
		dist = start.dist2(ending)
		anim_time = (min_anim_time + dist * 0.05).min(max_anim_time)
		print "distance between start and end {dist}"
		print "anim_time = {anim_time}"


	end

	# First, animate the fork to make it look like the avocado is thrown
	fun pre_anim_fork do

		# Initial angle we want the fork to have
		# TODO: maybe take in account the distance the avocado has to travel ?
		var fangle = -pi/2.0
		var s = leader.fork.sprite
		assert s != null
		var half = pre_anim_time / 2.0
		if time < half then
			s.rotation = 0.0 + fangle * (time / half).min(1.0)
		else
			s.rotation = fangle - fangle * ((time - half) / half).min(1.0)
		end
	end

	# Make the fork go and pick the next avocado
	fun animate_fork(dt: Float)
	do
		# first, we want to take the fork half the angle it needs to end
		# this will look smoother
		var actual_time = time - pre_anim_time

		# smooth angle time
		var smt = anim_time / 7.0
		# half angle
		var ha = angle / 2.0

		# Don't know how to name it I just use it too much in this function
		var a = (anim_time - (2.0*smt)) / 2.0

		if actual_time < smt then
			leader.fork.sprite.rotation = ha * (actual_time / smt)
		else if actual_time < anim_time and actual_time > anim_time - smt then
			leader.fork.sprite.rotation = ha - (1.0 - ((anim_time - actual_time) / smt) ) * ha
		else if actual_time >= smt and actual_time <= anim_time / 2.0 then
			var nt = (actual_time - smt) / a
			leader.fork.sprite.rotation = ha + ha * (nt).min(1.0)
			leader.fork.center.x = nt.qerp(fork_start.x, fcurve_point.x, fork_end.x)
			leader.fork.center.y = nt.qerp(fork_start.y, fcurve_point.y, fork_end.y)
		else if actual_time >= anim_time / 2.0 and actual_time <= anim_time - smt then
			var nt = (actual_time - smt -a) / a
			leader.fork.sprite.rotation = angle - ha * (nt)
			leader.fork.center.x = nt.qerp(fork_end.x, fcurve_point.x, fork_start.x)
			leader.fork.center.y = nt.qerp(fork_end.y, fcurve_point.y, fork_start.y)
		end

		# Middle of the animation, we need to switch the current_avocado of the leader
		# But we  also need to insert a placeholder with no sprite while the
		# current_avocado is in the air
		if actual_time - dt < anim_time / 2.0 and actual_time > anim_time / 2.0 then
			index = leader.avocados.index_of(leader.next_avocado)
			var c = leader.next_avocado.center
			var p = new Point3d[Float](c.x, c.y, c.z)
			var ph = new SoftAvocado(app.level.as(not null), p)
			leader.avocados[index] = ph
			app.sprites.remove(ph.sprite)
			leader.current_avocado = leader.next_avocado
		end
		# Make the avocado follow the fork
		if actual_time > anim_time / 2.0 then
			leader.update_current_avocado
		end
	end

	# Make the avocado take its new place
	fun animate_avocado do
		# Normalized time
		var nt = (time - pre_anim_time) / (anim_time)
		avocado.center.x = nt.qerp(start.x, curve_point.x, ending.x)
		avocado.center.y = nt.qerp(start.y, curve_point.y, ending.y)
	end

	# Instrument the animation
	fun update(dt: Float) do
		time += dt
		# pre animating fork for the throw
		if time < pre_anim_time then
			pre_anim_fork
			return
		end

		# we just finished the pre animation, time to throw the avocado
		if time - dt < pre_anim_time and time > pre_anim_time then
			app.sprites.add(avocado.sprite.as(not null))
		end
		if time < anim_time + pre_anim_time then
			animate_avocado
			animate_fork(dt)
		else
			leader.avocados[index] = avocado
			leader.next_avocado = null
			app.switching = null
		end
	end

end

redef class Leader
	# The fork of the devil, need a direct ref to change the sprite
	var fork: DevilFork is noautoinit

	redef fun draw_order do return 2

	init do
		if app.assets_level_index != 3 then
			var p = new OffsetPoint3d(center, 0.0, 1.1, 0.1)
			var head = new DevilHead(level, p)
			var p2 = new Point3d[Float](center.x -0.25, center.y + 1.5, center.z +0.2)
			var fork = new DevilFork(level, p2)
			parts.add(head)
			parts.add(fork)
			self.fork = fork
		end
	end

	redef fun texture do if app.assets_level_index == 3 then
			return app.assets.avocado_leader
		else return app.assets.devil_body

	redef fun dead=(value) do
		super
		app.sprites.remove_all sprite
	end

	redef fun collect(ingredient)
	do
		super

		# TODO move to ui_sprites
		ingredient.sprite.center.y -= 100.0
	end

	redef fun move(dt, dx, dy)
	do
		super

		var sprite = sprite
		assert sprite != null

		var rotation = 0.0
		if dx == 0.0 then
			rotation = (heading*dt*move_speed) / 4.0
			if heading == -1.0 and sprite.rotation > 0.0 then
				if sprite.rotation + rotation < 0.0 then
					sprite.rotation = 0.0
				else
					sprite.rotation += rotation
				end
			else if heading == 1.0 and sprite.rotation < 0.0 then
				if sprite.rotation + rotation > 0.0 then
					sprite.rotation = 0.0
				else
					sprite.rotation += rotation
				end
			end
		else
			rotation = (dx*dt*move_speed)
			if sprite.rotation - rotation > pi / 12.0 then
				sprite.rotation = pi / 12.0
			else if sprite.rotation - rotation < -pi / 12.0 then
				sprite.rotation = -pi / 12.0
			else
				sprite.rotation -= rotation
			end

		end
		for p in parts do p.update(dt, dx, dy, self)
	end

	redef fun rotate(wise) do
		super
		if next_avocado != null then
			app.switching = new SwitchGesture(self)
		end
	end

	# Update the `current_avocado` on the fork
	fun update_current_avocado
	do
		var avocado = current_avocado
		if avocado != null then
			# Follow the fork and place at one end
			var fork_sprite = fork.sprite
			var avocado_sprite = avocado.sprite
			if fork_sprite == null or avocado_sprite == null then return

			var d_to_avocado = 1.1

			var ang = fork_sprite.rotation + 0.12*pi + pi/2.0
			avocado_sprite.center.x = fork.center.x + d_to_avocado * ang.cos
			avocado_sprite.center.y = fork.center.y + d_to_avocado * ang.sin
			avocado_sprite.center.z = fork.center.z + 0.1
			avocado_sprite.rotation = fork_sprite.rotation - 0.3*pi
			avocado_sprite.draw_order = fork_sprite.draw_order + 1
		end
	end
end

redef class Avocado
	# to switch the fork sprite when needed
	fun fork_texture: Texture do return app.assets.fork

	# Add a random time for the wobbling of the avocados
	var random_wobbling: Float is lazy do return 2.0 * pi.rand

	# Modifier for the max height wobbling of avocados
	var random_height_max: Float is lazy do return 3.0 + 1.0.rand * 5.0

	redef var draw_order = (center.z*10.0).to_i is lazy

	redef fun texture do return app.assets.avocado_soft
	init do
		sprite.scale *= 0.9 + 0.2.rand
		sprite.tint[0] = 0.8 + 0.2.rand
		sprite.tint[1] = 0.8 + 0.2.rand
		sprite.tint[2] = 0.8 + 0.2.rand
	end

	redef fun update(dt) do
		var t = app.sprites.time + random_wobbling
		var v = (2.0 * t).sin * pi / 6.0
		sprite.rotation = v
		center.y += (10.0 * t).sin.abs / random_height_max
	end
end

redef class SoftAvocado
	redef fun texture do return app.assets.avocado_soft
	redef fun fork_texture do return app.assets.fork_soft
end

redef class HardAvocado
	redef fun texture do return app.assets.avocado_hard
	redef fun fork_texture do return app.assets.fork_hard
end

redef class OldAvocado
	redef fun texture do return app.assets.avocado_old
	redef fun fork_texture do return app.assets.fork_old
end

redef class LawyerAvocado
	redef fun texture do return app.assets.avocado_lawyer
	redef fun fork_texture do return app.assets.fork_lawyer
end

redef class Cilantro
	redef fun texture do return app.assets.cilantro
end

redef class Lime
	redef fun texture do return app.assets.lime
end

redef class Tile
	redef fun texture do return app.assets.tile[app.assets_level_index]

	redef fun model do return app.model_tile.modulo(app.assets_level_index)
	private fun replaced_by_large_model: Bool
	do
		if app.assets_level_index == 0 then
			if center.x < -13.0 then return true # 1st platform of the 1st level
		end

		return false
	end

	redef fun splash
	do
		if not splashed then
			var actor = new Actor(app.model_splash, center)
			app.actors.add actor
			actors.add actor
		end

		super
	end
end

redef class GroundTile

	redef fun add_to_scene
	do
		if replaced_by_large_model then return
		super
	end
end

redef class GrowableTile
	redef fun texture do return app.assets.tile_growable[app.assets_level_index]

	redef fun add_to_scene
	do
		if not replaced_by_large_model then
			super
		end

		var actor = new Actor(app.model_growable, center)
		app.actors.add actor
		actors.add actor
	end
end

redef class TreeTile
	redef fun texture do return app.assets.tree
	redef fun model do return app.model_plant
	init do actors.first.roll = angle
end

redef class Door
	redef fun texture do return app.assets.closed_door

	redef fun open=(value)
	do
		super

		if value then
			sprite.texture = app.assets.open_door
		end
	end
end

redef class Ennemy
	redef fun stuck=(value) do sprite.rotation = -pi / 2.0
end

redef class Nacho
	redef fun texture do return app.assets.nacho
end

redef class Bean
	redef fun texture do return app.assets.bean
end

redef class Boss
	redef fun texture do return app.assets.devil_body
end

# Button to reload level
class ResetButton
	super Button

	redef fun hit do app.reset_level
end

# Button to load the next level
class NextLevelButton
	super Button

	redef fun hit do
		app.clear_story
		app.next_level
	end
end

class VirtualAvocado
	super ThrowableEntity

	redef fun texture do return app.assets.arrow

	fun simulate_throw(force, angle: Float): nullable Tile
	do
		# Clean last call
		landing_tile = null
		for s in app.virtual_trace do app.sprites.remove_all s

		throw(force, angle, app.level.leader)
		var dt = 1.0/60.0
		for i in  [0..120[ do
			move(dt, 0.0, 0.0)
			var tile = landing_tile
			if tile != null then return tile

			var j = i / 8
			if i % 8 == 7 and j < 16 then
				var s
				if app.virtual_trace.length > j then
					s = app.virtual_trace[j]
				else
					s = new Sprite(app.assets.trace, new Point3d[Float](0.0, 0.0, 0.0))
					s.scale = 0.02
					app.virtual_trace.add s
				end

				s.center.x = center.x
				s.center.y = center.y
				app.sprites.add s
			end
		end
		return null
	end

	private var landing_tile: nullable Tile = null

	redef fun land(tile) do landing_tile = tile
end

redef class LevelInfo
	redef fun load do
		var lvl = super
		var leader = lvl.leader
		return lvl
	end
end
